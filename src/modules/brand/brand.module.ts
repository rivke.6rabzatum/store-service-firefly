import { Module } from '@nestjs/common';
import { BrandService } from './brand.service';
import { BrandController } from './brand.controller';
import { Brand } from './entity/brand.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ModuleService } from '../module/module.service';
import { ModuleEntity } from '../module/entity/module.entity';
import { ProductsService } from '../products/products.service';
import { Product } from '../products/entity/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Brand, ModuleEntity, Product])],
  providers: [BrandService, ModuleService, ProductsService],
  controllers: [BrandController],
})
export class BrandModule {}
