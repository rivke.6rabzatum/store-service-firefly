import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ModuleService } from '../module/module.service';
import { CreateBrandDto, UpdateBrandDto } from './dto';
import { Brand } from './entity/brand.entity';

@Injectable()
export class BrandService {
  constructor(
    @InjectRepository(Brand)
    private readonly brandRepository: Repository<Brand>,
    private readonly moduleService: ModuleService,
  ) {}

  async createBrand(createBrandDto: CreateBrandDto) {
    const brand = this.brandRepository.create(createBrandDto as any);
    return await this.brandRepository.save(brand);
  }

  async updateBrand(id: string, updateBrandDto: UpdateBrandDto) {
    const brand = await this.brandRepository.findOne(id);
    if (!brand) throw new NotFoundException('Brand not found');
    const editedBrand = Object.assign(brand, updateBrandDto);
    return await this.brandRepository.save(editedBrand);
  }

  async getAllBrand() {
    return await this.brandRepository.find();
  }

  async getOneBrand(id: string) {
    const brand = await this.brandRepository.findOne(id);
    if (!brand) throw new NotFoundException('Brand not found');
    return brand;
  }

  async deleteOneBrand(id: string) {
    const brand = await this.brandRepository.findOne(id);
    if (!brand) throw new NotFoundException('Brand not found');
    this.moduleService.deleteByBrandIdAndModuleId(id);
    return await this.brandRepository.delete(id);
  }
}
