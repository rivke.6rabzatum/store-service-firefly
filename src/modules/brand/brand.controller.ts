import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BrandService } from './brand.service';
import { CreateBrandDto, UpdateBrandDto } from './dto';

@ApiTags('Brands')
@Controller({
  path: 'brands',
  version: '1',
})
export class BrandController {
  constructor(private readonly brandService: BrandService) {}

  @Post()
  async createBrand(@Body() createBrandDto: CreateBrandDto) {
    return this.brandService.createBrand(createBrandDto);
  }

  @Put()
  updatBrand(@Query('id') id: string, @Body() updateBrandDto: UpdateBrandDto) {
    return this.brandService.updateBrand(id, updateBrandDto);
  }

  @Get('allbrands')
  getAll() {
    return this.brandService.getAllBrand();
  }

  @Get()
  findOne(@Query('id') id: string) {
    return this.brandService.getOneBrand(id);
  }

  @Delete()
  deleteOne(@Query('id') id: string) {
    return this.brandService.deleteOneBrand(id);
  }
}
