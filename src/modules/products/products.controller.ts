import {
  Body,
  Controller,
  Delete,
  Get,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateProductDto, UpdateProductDto } from './dto';
import { ProductsService } from './products.service';

@ApiTags('Products')
@Controller({
  path: 'products',
  version: '1',
})
export class ProductsController {
  constructor(private readonly productService: ProductsService) {}

  @Post()
  async createProduct(@Body() createProducDto: CreateProductDto) {
    return this.productService.createProduct(createProducDto);
  }

  @Put()
  updateProduct(
    @Query('id') id: string,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    return this.productService.updateProduct(id, updateProductDto);
  }

  @Get('allproducts')
  getAll() {
    return this.productService.getAllProducts();
  }

  @Get()
  findOne(@Query('id') id: string) {
    return this.productService.getOneProducts(id);
  }

  @Delete('deleteproduct')
  deleteProduct(@Query('id') id: string) {
    return this.productService.deleteProduct(id);
  }

  @Put('activproduct')
  activProduct(@Query('id') id: string) {
    return this.productService.activOne(id);
  }

  @Put('updateModule')
  updateModuleId(@Query('id') id: string, @Query('module') moduleId: string) {
    return this.productService.updateModuleId(id, moduleId);
  }
}
