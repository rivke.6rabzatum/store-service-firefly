import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('products')
export class Product {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  title: string;

  @Column({ type: 'text' })
  image: string;

  @Column()
  description: string;

  @Column({ nullable: false, default: '0' })
  module_id: string;

  @Column()
  price: string;

  @Column({ nullable: false })
  box_price: string;

  @Column({ nullable: false })
  pack_price: string;

  @Column({ type: 'boolean', default: false })
  delete: boolean;
}
