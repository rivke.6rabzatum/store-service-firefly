import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateProductDto {
  @IsString()
  @IsNotEmpty()
  readonly title!: string;

  @IsString()
  @IsNotEmpty()
  readonly image!: string;

  @IsString()
  @IsNotEmpty()
  readonly description!: string;

  @IsString()
  @IsNotEmpty()
  readonly module_id?: string;

  @IsString()
  @IsNotEmpty()
  readonly price!: string;

  @IsString()
  readonly box_price?: string;

  @IsString()
  readonly pack_price?: string;
}
