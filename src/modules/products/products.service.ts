import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto, UpdateProductDto } from './dto';
import { Product } from './entity/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async createProduct(createProductDto: CreateProductDto) {
    const product = this.productRepository.create(createProductDto as any);
    return await this.productRepository.save(product);
  }

  async updateProduct(id: string, updateProductDto: UpdateProductDto) {
    const product = await this.productRepository.findOne(id);
    if (!product) throw new NotFoundException('Product not found');
    const editedProduct = Object.assign(product, updateProductDto);
    return await this.productRepository.save(editedProduct);
  }

  async getAllProducts() {
    return await this.productRepository.find();
  }

  async getOneProducts(id: string) {
    const product = await this.productRepository.findOne(id);
    if (!product) throw new NotFoundException('Product not found');
    return product;
  }

  async deleteOne(id: string) {
    const product = await this.productRepository.findOne(id);
    if (!product) throw new NotFoundException('Product not found');
    const changeDelete = Object.assign({ ...product, delete: true });
    return await this.productRepository.save(changeDelete);
  }

  async deleteProduct(id: string) {
    const product = await this.productRepository.findOne(id);
    if (!product) return console.log('PRODUCT NOT FOUND');
    return await this.productRepository.delete(id);
  }

  async deleteToModuleId(module_id: string) {
    const product = await this.productRepository.findOne({ module_id });
    if (!product) console.log('PRODUCT NOT FOUND');
    return await this.productRepository.delete({ module_id });
  }

  async activOne(id: string) {
    const product = await this.productRepository.findOne(id);
    if (!product) throw new NotFoundException('Product not found');
    const changeDelete = Object.assign({ ...product, delete: false });
    return await this.productRepository.save(changeDelete);
  }

  async updateModuleId(id: string, moduleId: string) {
    const product = await this.productRepository.findOne(id);
    if (!product) throw new NotFoundException('Product not found');
    const upModuleId = Object.assign({ ...product, module_id: moduleId });
    return await this.productRepository.save(upModuleId);
  }
}
