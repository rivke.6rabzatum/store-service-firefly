import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CategorysService } from './categorys.service';
import { CreateCategoryDto } from './dto';

@ApiTags('Categorys')
@Controller({
  path: 'category',
  version: '1',
})
export class CategorysController {
  constructor(private readonly categoryService: CategorysService) {}

  @Post()
  async createProduct(@Body() createCategoryDto: CreateCategoryDto) {
    return this.categoryService.create(createCategoryDto);
  }

  @Get('allcategorys')
  getAll() {
    return this.categoryService.getAll();
  }
}
