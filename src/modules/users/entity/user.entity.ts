import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 10, unique: true })
  user_name: string;

  @Column({ length: 100 })
  complete_name: string;

  @Column({ length: 100, unique: false })
  email: string;

  @Column({ length: 100 })
  password: string;

  @Column({ type: 'boolean', default: false })
  delete: boolean;

  @Column({ length: 30, default: 'USER' })
  rol: string;

  @Column({ length: 36 })
  client_id: string;
}
