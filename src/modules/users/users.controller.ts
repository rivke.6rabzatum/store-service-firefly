import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SignupDto, UpdateUserDto } from './dto';
import { UsersService } from './users.service';

@ApiTags('Users')
@Controller({
  path: 'users',
  version: '1',
})
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post()
  async signUp(@Body() signUpDto: SignupDto) {
    return this.userService.signUp(signUpDto);
  }

  @Put()
  updateUser(@Query('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.updateUser(id, updateUserDto);
  }

  @Get()
  findOne(@Query('id') id: string) {
    return this.userService.getUser(id);
  }

  @Get('allusers')
  getAllUser() {
    return this.userService.getAllUser();
  }

  @Delete('disableuser')
  disableUser(@Query('id') id: string) {
    return this.userService.disableUser(id);
  }

  @Delete('deleteUser')
  deleteUser(@Query('id') id: string) {
    return this.userService.deleteUser(id);
  }

  @Put('activuser')
  activUser(@Query('id') id: string) {
    return this.userService.activeUser(id);
  }
}
