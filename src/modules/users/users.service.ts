import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SignupDto, UpdateUserDto } from './dto';
import { EncoderService } from './encoder.service';
import { User } from './entity/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private encoderService: EncoderService,
  ) {}

  async signUp(signUpDto: SignupDto) {
    const { user_name, complete_name, email, rol, client_id } = signUpDto;
    let password = signUpDto.password;
    /*
    const hashedPassword = await this.encoderService.encodePassword(password);
    password = hashedPassword;*/
    const user = this.userRepository.create({
      user_name,
      complete_name,
      email,
      password,
      rol,
      client_id,
    } as any);
    return await this.userRepository.save(user);
  }

  async updateUser(id: string, updateUser: UpdateUserDto) {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('User not found');
    const editedTask = Object.assign(user, updateUser);
    return await this.userRepository.save(editedTask);
  }

  async getUser(id: string) {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('User not found');
    return user;
  }

  async getAllUser() {
    return await this.userRepository.find();
  }

  async disableUser(id: string) {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('User not found');
    const changeDelete = Object.assign({ ...user, delete: true });
    return await this.userRepository.save(changeDelete);
  }

  async activeUser(id: string) {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('User not found');
    const changeDelete = Object.assign({ ...user, delete: false });
    return await this.userRepository.save(changeDelete);
  }

  async deleteUser(id: string) {
    const user = await this.userRepository.findOne(id);
    if (!user) throw new NotFoundException('User not found');
    return await this.userRepository.delete(id);
  }
}
