import {
  IsEmail,
  IsNotEmpty,
  IsString,
  Length,
  Matches,
} from 'class-validator';

export class UpdateUserDto {
  @IsNotEmpty()
  @IsString()
  @Length(2, 30)
  user_name: string;

  @IsNotEmpty()
  @IsString()
  @Length(6, 60)
  complete_name: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(8)
  password: string;

  @IsNotEmpty()
  @IsString()
  rol: string;

  @IsString()
  client_id: string;
}
