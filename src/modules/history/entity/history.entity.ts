import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('history')
export class History {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  user_id: string;

  @Column()
  product_id: string;

  @Column()
  product_name: string;

  @Column()
  product_quantity: string;

  @Column()
  product_price: string;

  @Column()
  total_price: string;

  @Column({ type: 'boolean', default: false })
  status?: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  create_at?: Date;

  @Column()
  product_unit?: string;

  @Column()
  order_num?: string;
}
