import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateHistoryDto, UpdateHistoryDto } from './dto';
import { History } from './entity/history.entity';

@Injectable()
export class HistoryService {
  constructor(
    @InjectRepository(History)
    private readonly historyRepository: Repository<History>,
  ) {}

  async createHistory(createHistoryDto: CreateHistoryDto) {
    const history = this.historyRepository.create(createHistoryDto as any);
    return await this.historyRepository.save(history);
  }

  async updateHistory(id: string, updateHistoryDto: UpdateHistoryDto) {
    const history = await this.historyRepository.findOne(id);
    if (!history) throw new NotFoundException('History not found');
    const editedHistory = Object.assign(history, updateHistoryDto);
    return await this.historyRepository.save(editedHistory);
  }

  async updateState(id: string) {
    const history = await this.historyRepository.find({
      where: { user_id: id, status: false },
    });
    if (!history) throw new NotFoundException('History not found');
    let change = history;
    history.forEach((e, i) => {
      change[i].status = true;
    });
    return await this.historyRepository.save(history);
  }

  async getAllComplete(id: string) {
    const history = await this.historyRepository.find({
      where: { user_id: id, status: true },
      order: { create_at: 'ASC' },
    });
    return history;
  }

  async getAllPendant(id: string) {
    const history = await this.historyRepository.find({
      where: { user_id: id, status: false },
      order: { create_at: 'ASC' },
    });
    return history;
  }

  async deleteHistory(id: string) {
    const history = await this.historyRepository.findOne(id);
    if (!history) throw new NotFoundException('History not found');
    return await this.historyRepository.delete(id);
  }
}
