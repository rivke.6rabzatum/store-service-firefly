import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { CreateHistoryDto, UpdateHistoryDto } from './dto';
import { HistoryService } from './history.service';

@ApiTags('History')
@Controller({
  path: 'history',
  version: '1',
})
export class HistoryController {
  constructor(private readonly historyService: HistoryService) {}

  //@ApiBody({ type: [CreateHistoryDto] })
  @Post()
  async createHistory(@Body() createHistoryDto: CreateHistoryDto) {
    return await this.historyService.createHistory(createHistoryDto);
  }

  @Put()
  async updateHistory(
    @Query('id') id: string,
    @Body() updateHistoryDto: UpdateHistoryDto,
  ) {
    return await this.historyService.updateHistory(id, updateHistoryDto);
  }

  @Put('state')
  async updateState(@Query('user_id') user_id: string) {
    return await this.historyService.updateState(user_id);
  }

  @Get('complete')
  async getAllComplete(@Query('user_id') user_id: string) {
    return await this.historyService.getAllComplete(user_id);
  }

  @Get('pendant')
  async getAllPendant(@Query('user_id') user_id: string) {
    return await this.historyService.getAllPendant(user_id);
  }

  @Delete()
  async deleteHistory(@Query('id') id: string) {
    return await this.historyService.deleteHistory(id);
  }
}
