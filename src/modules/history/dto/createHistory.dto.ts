import { IsNotEmpty, IsString } from 'class-validator';

export class CreateHistoryDto {
  @IsString()
  @IsNotEmpty()
  readonly user_id: string;

  @IsString()
  @IsNotEmpty()
  readonly product_id: string;

  @IsString()
  @IsNotEmpty()
  readonly product_name: string;

  @IsString()
  @IsNotEmpty()
  readonly product_quantity: string;

  @IsString()
  @IsNotEmpty()
  readonly product_price: string;

  @IsString()
  @IsNotEmpty()
  readonly total_price: string;

  @IsString()
  @IsNotEmpty()
  readonly product_unit?: string;

  @IsString()
  readonly order_num?: string;
}
