import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('module')
export class ModuleEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 30, nullable: false, unique: false })
  name: string;

  @Column({ type: 'uuid' })
  brand_id: string;

  @Column({ type: 'text' })
  image?: string;
}
