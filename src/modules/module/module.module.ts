import { Module } from '@nestjs/common';
import { ModuleService } from './module.service';
import { ModuleController } from './module.controller';
import { ModuleEntity } from './entity/module.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from '../products/entity/product.entity';
import { ProductsService } from '../products/products.service';

@Module({
  imports: [TypeOrmModule.forFeature([ModuleEntity, Product])],
  providers: [ModuleService, ProductsService],
  controllers: [ModuleController],
})
export class ModuleModule {}
