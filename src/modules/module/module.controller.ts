import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateModuleDto, UpdateModuleDto } from './dto';
import { ModuleService } from './module.service';

@ApiTags('Modules')
@Controller({
  path: 'modules',
  version: '1',
})
export class ModuleController {
  constructor(private readonly moduleService: ModuleService) {}

  @Post()
  async createModule(@Body() createModuleDto: CreateModuleDto) {
    return this.moduleService.createModule(createModuleDto);
  }

  @Put()
  updateModule(
    @Query('id') id: string,
    @Body() updateModuleDto: UpdateModuleDto,
  ) {
    return this.moduleService.updateModule(id, updateModuleDto);
  }

  @Get()
  findOne(@Query('id') id: string) {
    return this.moduleService.getOneModule(id);
  }

  @Get('allmodules')
  getAll() {
    return this.moduleService.getAllModules();
  }

  @Delete()
  deleteOne(@Query('id') id: string) {
    return this.moduleService.deleteOneModule(id);
  }
}
