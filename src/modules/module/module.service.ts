import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProductsService } from '../products/products.service';
import { CreateModuleDto, UpdateModuleDto } from './dto';
import { ModuleEntity } from './entity/module.entity';

@Injectable()
export class ModuleService {
  constructor(
    @InjectRepository(ModuleEntity)
    private readonly moduleRepository: Repository<ModuleEntity>,
    private readonly producstService: ProductsService,
  ) {}

  async createModule(createModuleDto: CreateModuleDto) {
    const modules = this.moduleRepository.create(createModuleDto as any);
    return await this.moduleRepository.save(modules);
  }

  async updateModule(id: string, updateModuleDto: UpdateModuleDto) {
    const modules = await this.moduleRepository.findOne(id);
    if (!modules) throw new NotFoundException('Module not found');
    const editedModule = Object.assign(modules, updateModuleDto);
    return await this.moduleRepository.save(editedModule);
  }

  async getAllModules() {
    return await this.moduleRepository.find();
  }

  async getOneModule(id: string) {
    const modules = await this.moduleRepository.findOne(id);
    if (!modules) throw new NotFoundException('Module not found');
    return modules;
  }

  async deleteOneModule(id: string) {
    const module = await this.moduleRepository.findOne(id);
    if (!module) throw new NotFoundException('Module not found');
    this.producstService.deleteToModuleId(id);
    return await this.moduleRepository.delete(id);
  }

  async deleteByBrandIdAndModuleId(brand_id: string) {
    const module = await this.moduleRepository.findOne({ brand_id });
    if (!module) return console.log('MODULE NOT FOUND');
    this.producstService.deleteToModuleId(module.id);
    return await this.moduleRepository.delete({ brand_id });
  }
}
